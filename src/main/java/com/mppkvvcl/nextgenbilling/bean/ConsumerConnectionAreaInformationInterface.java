package com.mppkvvcl.nextgenbilling.bean;

import java.util.Date;

/**
 * Created by RUPALI on 9/22/2017.
 */
public interface ConsumerConnectionAreaInformationInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public String getConsumerNo();

    public void setConsumerNo(String consumerNo);

    public String getDtrName();

    public void setDtrName(String dtrName);

    public String getPoleNo();

    public void setPoleNo(String poleNo);

    public String getPoleLatitude();

    public void setPoleLatitude(String poleLatitude);

    public String getPoleLongitude();

    public void setPoleLongitude(String poleLongitude);

    public String getFeederName();

    public void setFeederName(String feederName);

    public long getPoleDistance();

    public void setPoleDistance(long poleDistance);

    public String getAreaStatus();

    public void setAreaStatus(String areaStatus);

    public String getGroupNo();

    public void setGroupNo(String groupNo);

    public String getReadingDiaryNo();

    public void setReadingDiaryNo(String readingDiaryNo);

    public String getNeighbourConnectionNo();

    public void setNeighbourConnectionNo(String neighbourConnectionNo);

    public Date getSurveyDate();

    public void setSurveyDate(Date surveyDate);

    public void setCreatedOn(Date createdOn);

    public Date getCreatedOn();

    public void setCreatedBy(String createdBy);

    public String getCreatedBy();

    public void setUpdatedOn(Date updatedOn);

    public Date getUpdatedOn();

    public void setUpdatedBy(String updatedBy);

    public String getUpdatedBy();


}
