package com.mppkvvcl.nextgenbilling.bean;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface MeterCTRMappingInterface extends BeanInterface {

    public static final String STATUS_ACTIVE = "ACTIVE";

    public static final String STATUS_INACTIVE = "INACTIVE";

    public long getId();

    public void setId(long id);

    public String getMeterIdentifier();

    public void setMeterIdentifier(String meterIdentifier);

    public String getCtrIdentifier();

    public void setCtrIdentifier(String ctrIdentifier);

    public BigDecimal getOverallMf();

    public void setOverallMf(BigDecimal overallMf);

    public String getMappingStatus();

    public void setMappingStatus(String mappingStatus);

    public Date getInstallationDate();

    public void setInstallationDate(Date installationDate);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getUpdatedBy();

    public void setUpdatedBy(String updatedBy);

    public Date getUpdatedOn();

    public void setUpdatedOn(Date updatedOn);

}
