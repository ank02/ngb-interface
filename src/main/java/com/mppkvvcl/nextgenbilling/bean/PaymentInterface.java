package com.mppkvvcl.nextgenbilling.bean;

import java.util.Date;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface PaymentInterface extends BeanInterface{

    public static final String PAY_MODE_CHEQUE = "CHEQUE";

    public static final String PAY_MODE_BANKERCHEQUE = "BANKERCHEQUE";

    public static final String PAY_MODE_DD = "DD";

    public static final String PAY_MODE_CASH = "CASH";

    public static final boolean POSTED_TRUE = true;

    public static final boolean POSTED_FALSE = false;

    public static final boolean DELETED_TRUE = true;

    public static final boolean DELETED_FALSE = false;

    public static final boolean ONLINE_FALSE = false;

    public static final String SOURCE_MANUAL = "MANUAL";

    public static final long MINIMUM_PAYMENT_AMOUNT = 1;

    public long getId();

    public void setId(long id);

    public String getSource();

    public void setSource(String source);

    public boolean isOnline();

    public void setOnline(boolean online);

    public String getLocationCode();

    public void setLocationCode(String locationCode);

    public String getConsumerNo();

    public void setConsumerNo(String consumerNo);

    public Date getPunchingDate();

    public void setPunchingDate(Date punchingDate);

    public Date getPayDate();

    public void setPayDate(Date payDate);

    public long getAmount();

    public void setAmount(long amount);

    public String getPayMode();

    public void setPayMode(String payMode);

    public String getPayWindow();

    public void setPayWindow(String payWindow);

    public String getCacNo();

    public void setCacNo(String cacNo);

    public boolean isDeleted();

    public void setDeleted(boolean deleted);

    public boolean isPosted();

    public void setPosted(boolean posted);

    public String getPostingBillMonth();

    public void setPostingBillMonth(String postingBillMonth);

    public Date getPostingDate();

    public void setPostingDate(Date postingDate);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getUpdatedBy();

    public void setUpdatedBy(String updatedBy);

    public Date getUpdatedOn();

    public void setUpdatedOn(Date updatedOn);

}
