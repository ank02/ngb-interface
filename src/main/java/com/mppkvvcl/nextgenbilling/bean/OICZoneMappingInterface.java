package com.mppkvvcl.nextgenbilling.bean;

public interface OICZoneMappingInterface extends BeanInterface{
    public long getId();

    public void setId(long id);

    public UserDetailInterface getUserDetail();

    public void setUserDetail(UserDetailInterface userDetail);

    public ZoneInterface getZone();

    public void setZone(ZoneInterface zone);
}
