package com.mppkvvcl.nextgenbilling.bean;

import java.math.BigDecimal;

/**
 * Created by RUPALI on 9/22/2017.
 */
public interface AdjustmentRangeInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public int getCode();

    public void setCode(int code);

    public BigDecimal getStart();

    public void setStart(BigDecimal start);

    public BigDecimal getEnd();

    public void setEnd(BigDecimal end);

    public int getMaxPriority();

    public void setMaxPriority(int maxPriority);
}
