package com.mppkvvcl.nextgenbilling.bean;

import java.math.BigDecimal;

public interface MinimumChargeInterface extends BeanInterface {

    public long getId() ;

    public void setId(long id) ;

    public long getTariffId() ;

    public void setTariffId(long tariffId) ;

    public long getSubcategoryCode() ;

    public void setSubcategoryCode(long subcategoryCode);

    public BigDecimal getAmount();

    public void setAmount(BigDecimal amount);

    public String getAppVersion() ;

    public void setAppVersion(String appVersion) ;
}
