package com.mppkvvcl.nextgenbilling.bean;

import java.util.Date;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface MeterModemMappingInterface extends BeanInterface {

    public static final String STATUS_ACTIVE = "ACTIVE";

    public static final String STATUS_INACTIVE = "INACTIVE";

    public long getId();

    public void setId(long id);

    public String getMeterIdentifier();

    public void setMeterIdentifier(String meterIdentifier);

    public String getModemNo();

    public void setModemNo(String modemNo);

    public String getSimNo();

    public void setSimNo(String simNo);

    public String getStatus();

    public void setStatus(String status);

    public Date getStartDate();

    public void setStartDate(Date startDate);

    public Date getEndDate();

    public void setEndDate(Date endDate);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getUpdatedBy();

    public void setUpdatedBy(String updatedBy);

    public Date getUpdatedOn();

    public void setUpdatedOn(Date updatedOn);

}
