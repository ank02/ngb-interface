package com.mppkvvcl.nextgenbilling.bean;

public interface SECircleMappingInterface extends BeanInterface{
    public long getId();

    public void setId(long id);

    public UserDetailInterface getUserDetail();

    public void setUserDetail(UserDetailInterface userDetail);

    public CircleInterface getCircle();

    public void setCircle(CircleInterface circle);
}
