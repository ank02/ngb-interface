package com.mppkvvcl.nextgenbilling.bean;

import java.math.BigDecimal;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface SurchargeInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public long getTariffId();

    public void setTariffId(long tariffId);

    public long getSubcategoryCode();

    public void setSubcategoryCode(long subcategoryCode);

    public BigDecimal getOutstandingAmount();

    public void setOutstandingAmount(BigDecimal outstandingAmount);

    public BigDecimal getMinimumSuchargeAmount();

    public void setMinimumSuchargeAmount(BigDecimal minimumSuchargeAmount);

    public String getRate();

    public void setRate(String rate);

    public BigDecimal getMultiplier();

    public void setMultiplier(BigDecimal multiplier);

    public int getRoundingModeCode();

    public void setRoundingModeCode(int roundingModeCode);

    public RoundingModeInterface getRoundingModeInterface();

    public void setRoundingModeInterface(RoundingModeInterface roundingModeInterface);
}
