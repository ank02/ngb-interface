package com.mppkvvcl.nextgenbilling.bean;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by ANSHIKA on 22-09-2017.
 * Latest by Nitish on 23-10-2017
 */
public interface SubCategoryInterface extends BeanInterface {

    public static final String PREMISE_TYPE_RURAL = "RURAL";

    public static final String PREMISE_TYPE_URBAN = "URBAN";

    public long getId();

    public void setId(long id);

    public long getTariffId();

    public void setTariffId(long tariffId);

    public long getCode();

    public void setCode(long code);

    public String getSlabId();

    public void setSlabId(String slabId);

    public String getDescription();

    public void setDescription(String description);

    public BigDecimal getStartContractDemandKW();

    public void setStartContractDemandKW(BigDecimal startContractDemandKW);

    public BigDecimal getEndContractDemandKW();

    public void setEndContractDemandKW(BigDecimal endContractDemandKW);

    public BigDecimal getStartConnectedLoadKW();

    public void setStartConnectedLoadKW(BigDecimal startConnectedLoadKW);

    public BigDecimal getEndConnectedLoadKW();

    public void setEndConnectedLoadKW(BigDecimal endConnectedLoadKW);

    public String getPremiseType();

    public void setPremiseType(String premiseType);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public String getUpdatedBy();

    public void setUpdatedBy(String updatedBy);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public Date getUpdatedOn();

    public void setUpdatedOn(Date updatedOn);

    public String getApplicantType();

    public void setApplicantType(String applicantType);

    public List<SurchargeInterface> getSurchargeInterfaces();

    public void setSurchargeInterfaces(List<SurchargeInterface> surchargeInterfaces);

    public List<SlabInterface> getSlabInterfaces();

    public void setSlabInterfaces(List<SlabInterface> slabInterfaces);

    public FCAInterface getFcaInterface();

    public void setFcaInterface(FCAInterface fcaInterface);

    public PowerFactorInterface getPowerFactorInterface();

    public void setPowerFactorInterface(PowerFactorInterface powerFactorInterface);

    public IrrigationSchemeInterface getIrrigationSchemeInterface();

    public void setIrrigationSchemeInterface(IrrigationSchemeInterface irrigationSchemeInterface);

    public AgricultureUnitInterface getAgricultureUnitInterface();

    public void setAgricultureUnitInterface(AgricultureUnitInterface agricultureUnitInterface);

    public List<SlabInterface> getAlternateSlabInterfaces();

    public void setAlternateSlabInterfaces(List<SlabInterface> alternateSlabInterfaces);

    public MeterRentInterface getMeterRentInterface();

    public void setMeterRentInterface(MeterRentInterface meterRentInterface);

    public List<ElectricityDutyInterface> getElectricityDutyInterfaces();

    public void setElectricityDutyInterfaces(List<ElectricityDutyInterface> electricityDutyInterfaces);

    public void setMinimumChargeInterface(MinimumChargeInterface minimumChargeInterface);

    public MinimumChargeInterface getMinimumChargeInterface();
}
