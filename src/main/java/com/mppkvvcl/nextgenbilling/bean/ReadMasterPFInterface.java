package com.mppkvvcl.nextgenbilling.bean;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface ReadMasterPFInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public long getReadMasterId();

    public void setReadMasterId(long readMasterId);

    public BigDecimal getMeterPF();

    public void setMeterPF(BigDecimal meterPF);

    public BigDecimal getBillingPF();

    public void setBillingPF(BigDecimal billingPF);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getUpdatedBy();

    public void setUpdatedBy(String updatedBy);

    public Date getUpdatedOn();

    public void setUpdatedOn(Date updatedOn);

}
