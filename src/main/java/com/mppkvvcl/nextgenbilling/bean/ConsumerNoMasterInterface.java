package com.mppkvvcl.nextgenbilling.bean;

import java.util.Date;

/**
 * Created by RUPALI on 9/22/2017.
 */
public interface ConsumerNoMasterInterface extends BeanInterface{

    public static final String STATUS_ACTIVE = "ACTIVE";

    public static final String STATUS_INACTIVE = "INACTIVE";

    public long getId();

    public void setId(long id);

    public String getLocationCode();

    public void setLocationCode(String locationCode);

    public String getGroupNo();

    public void setGroupNo(String groupNo);

    public String getConsumerNo();

    public void setConsumerNo(String consumerNo);

    public String getStatus();

    public void setStatus(String status);

    public String getOldLocationCode();

    public void setOldLocationCode(String oldLocationCode);

    public String getOldGroupNo();

    public void setOldGroupNo(String oldGroupNo);

    public String getOldServiceNoOne();

    public void setOldServiceNoOne(String oldServiceNoOne);

    public String getOldServiceNoTwo();

    public void setOldServiceNoTwo(String oldServiceNoTwo);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getUpdatedBy();

    public void setUpdatedBy(String updatedBy);

    public Date getUpdatedOn();

    public void setUpdatedOn(Date updatedOn);


}
