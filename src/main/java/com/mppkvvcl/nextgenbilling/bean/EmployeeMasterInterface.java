package com.mppkvvcl.nextgenbilling.bean;

import java.util.Date;

/**
 * Created by RUPALI on 9/22/2017.
 */
public interface EmployeeMasterInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public String getEmployeeNo();

    public void setEmployeeNo(String employeeNo);

    public String getCompany();

    public void setCompany(String company);

    public String getType();

    public void setType(String type);

    public String getStatus();

    public void setStatus(String status);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getUpdatedOn();

    public void setUpdatedOn(Date updatedOn);

    public String getUpdatedBy();

    public void setUpdatedBy(String updatedBy);


}
