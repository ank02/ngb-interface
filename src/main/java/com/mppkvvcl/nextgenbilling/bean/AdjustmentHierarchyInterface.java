package com.mppkvvcl.nextgenbilling.bean;

/**
 * Created by RUPALI on 9/22/2017.
 */
public interface AdjustmentHierarchyInterface extends BeanInterface{
    public long getId();
    public void setId(long id);
    public String getRole();
    public void setRole(String role);
    public int getPriority();
    public void setPriority(int priority);
}
