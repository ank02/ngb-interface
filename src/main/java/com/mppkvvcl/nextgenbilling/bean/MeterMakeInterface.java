package com.mppkvvcl.nextgenbilling.bean;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface MeterMakeInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public String getMake();

    public void setMake(String make);
}
