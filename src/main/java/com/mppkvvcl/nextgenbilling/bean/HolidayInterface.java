package com.mppkvvcl.nextgenbilling.bean;

import java.util.Date;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface HolidayInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public Date getDate();

    public void setDate(Date date);

    public String getDescription();

    public void setDescription(String description);
}
