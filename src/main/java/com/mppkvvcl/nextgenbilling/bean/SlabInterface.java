package com.mppkvvcl.nextgenbilling.bean;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface SlabInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public long getTariffId();

    public void setTariffId(long tariffId);

    public String getSlabId();

    public void setSlabId(String slabId);

    public long getSlabStart();

    public void setSlabStart(long slabStart);

    public long getSlabEnd();

    public void setSlabEnd(long slabEnd);

    public BigDecimal getRate();

    public void setRate(BigDecimal rate);

    public BigDecimal getFixedChargeUrban();

    public void setFixedChargeUrban(BigDecimal fixedChargeUrban);

    public BigDecimal getFixedChargeRural();

    public void setFixedChargeRural(BigDecimal fixedChargeRural);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getUpdatedBy();

    public void setUpdatedBy(String updatedBy);

    public Date getUpdatedOn();

    public void setUpdatedOn(Date updatedOn);
}
