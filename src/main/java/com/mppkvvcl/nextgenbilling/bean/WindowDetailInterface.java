package com.mppkvvcl.nextgenbilling.bean;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface WindowDetailInterface extends BeanInterface{
    public long getId();

    public void setId(long id);

    public String getLocationCode();

    public void setLocationCode(String locationCode);

    public String getName();

    public void setName(String name);

}
