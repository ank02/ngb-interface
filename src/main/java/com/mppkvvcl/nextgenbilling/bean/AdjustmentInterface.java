package com.mppkvvcl.nextgenbilling.bean;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by RUPALI on 9/22/2017.
 */
public interface AdjustmentInterface extends BeanInterface {
    
    public static final String APPROVED = "APPROVED";

    public static final String PENDING = "PENDING";

    public static final String REJECTED = "REJECTED";

    public static final boolean POSTED_TRUE = true;

    public static final boolean POSTED_FALSE = false;

    public static final boolean DELETED_TRUE = true;

    public static final boolean DELETED_FALSE = false;

    public static final String CODE_RC_DC = "RC-DC";
    
    public long getId();

    public void setId(long id);

    public int getCode();

    public void setCode(int code);

    public String getConsumerNo();

    public void setConsumerNo(String consumerNo);

    public String getLocationCode();

    public void setLocationCode(String locationCode);

    public BigDecimal getAmount();

    public void setAmount(BigDecimal amount);

    public boolean isPosted();

    public void setPosted(boolean posted);

    public String getPostingBillMonth();

    public void setPostingBillMonth(String postingBillMonth);

    public Date getPostingDate();

    public void setPostingDate(Date postingDate);

    public boolean isDeleted();

    public void setDeleted(boolean deleted);

    public String getApprovalStatus();

    public void setApprovalStatus(String approvalStatus);

    public long getRangeId();

    public void setRangeId(long rangeId);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getUpdatedBy();

    public void setUpdatedBy(String updatedBy);

    public Date getUpdatedOn();

    public void setUpdatedOn(Date updatedOn);

    public String getRemark();

    public void setRemark(String remark);


}
