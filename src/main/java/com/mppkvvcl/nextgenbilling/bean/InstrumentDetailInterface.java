package com.mppkvvcl.nextgenbilling.bean;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface InstrumentDetailInterface extends BeanInterface {

    public static final boolean DISHONOURED_TRUE = true;

    public static final boolean DISHONOURED_FALSE = false;

    public long getId();

    public void setId(long id);

    public String getPayMode();

    public void setPayMode(String payMode);

    public String getBankName();

    public void setBankName(String bankName);

    public String getInstrumentNo();

    public void setInstrumentNo(String instrumentNo);

    public Date getInstrumentDate();

    public void setInstrumentDate(Date instrumentDate);

    public String getMicrCode();

    public void setMicrCode(String micrCode);

    public BigDecimal getAmount();

    public void setAmount(BigDecimal amount);

    public boolean isDishonoured();

    public void setDishonoured(boolean dishonoured);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getUpdatedBy();

    public void setUpdatedBy(String updatedBy);

    public Date getUpdatedOn();

    public void setUpdatedOn(Date updatedOn);
}
