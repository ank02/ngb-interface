package com.mppkvvcl.nextgenbilling.bean;

/**
 * Created by RUPALI on 9/22/2017.
 */
public interface AdjustmentProfileInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public long getAdjustmentId();

    public void setAdjustmentId(long adjustmentId);

    public boolean getStatus();

    public void setStatus(boolean status);

    public String getApprover();

    public void setApprover(String approver);

    public String getLocationCode();

    public void setLocationCode(String locationCode);

    public String getRemark();

    public void setRemark(String remark);


}
