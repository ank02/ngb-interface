package com.mppkvvcl.nextgenbilling.bean;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface XrayConnectionInformationInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public String getConsumerNo();

    public void setConsumerNo(String consumerNo);

    public BigDecimal getXrayLoad();

    public void setXrayLoad(BigDecimal xrayLoad);

    public long getNoOfDentalXrayMachine();

    public void setNoOfDentalXrayMachine(long noOfDentalXrayMachine);

    public long getNoOfSinglePhaseXrayMachine();

    public void setNoOfSinglePhaseXrayMachine(long noOfSinglePhaseXrayMachine);

    public long getNoOfThreePhaseXrayMachine();

    public void setNoOfThreePhaseXrayMachine(long noOfThreePhaseXrayMachine) ;

    public String getStatus();

    public void setStatus(String status);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getUpdatedOn();

    public void setUpdatedOn(Date updatedOn);

    public String getUpdatedBy();

    public void setUpdatedBy(String updatedBy);

}
