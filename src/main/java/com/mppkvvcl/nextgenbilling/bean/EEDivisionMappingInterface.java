package com.mppkvvcl.nextgenbilling.bean;

public interface EEDivisionMappingInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public UserDetailInterface getUserDetail();

    public void setUserDetail(UserDetailInterface userDetail);

    public DivisionInterface getDivision();

    public void setDivision(DivisionInterface division);
}
