package com.mppkvvcl.nextgenbilling.bean;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface ZoneInterface extends BeanInterface{
    public long getId();

    public void setId(long id);

    public String getCode();

    public void setCode(String code);

    public String getName();

    public void setName(String name);

    public DivisionInterface getDivision();

    public void setDivision(DivisionInterface divisionInterface);
}
