package com.mppkvvcl.nextgenbilling.bean;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by RUPALI on 9/22/2017.
 */
public interface ConsumerMeterMappingInterface extends BeanInterface {

    public static final String STATUS_ACTIVE = "ACTIVE";

    public static final String STATUS_INACTIVE = "INACTIVE";

    public long getId();

    public void setId(long id);

    public String getConsumerNo();

    public void setConsumerNo(String consumerNo);

    public String getMeterIdentifier();

    public void setMeterIdentifier(String meterIdentifier);

    public String getMeterSerialNo();

    public void setMeterSerialNo(String meterSerialNo);

    public String getMeterMake();

    public void setMeterMake(String meterMake);

    public BigDecimal getStartRead();

    public void setStartRead(BigDecimal startRead);

    public BigDecimal getFinalRead();

    public void setFinalRead(BigDecimal finalRead);

    public String getMappingStatus();

    public void setMappingStatus(String mappingStatus);

    public Date getInstallationDate();

    public void setInstallationDate(Date installationDate);

    public String getInstallationBillMonth();

    public void setInstallationBillMonth(String installationBillMonth);

    public Date getRemovalDate();

    public void setRemovalDate(Date removalDate);

    public String getRemovalBillMonth();

    public void setRemovalBillMonth(String removalBillMonth);

    public String getRemark();

    public void setRemark(String remark);

    public String getInactiveRemark();

    public void setInactiveRemark(String inactiveRemark);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getUpdatedBy();

    public void setUpdatedBy(String updatedBy);

    public Date getUpdatedOn();

    public void setUpdatedOn(Date updatedOn);

    public MeterMasterInterface getMeterMaster();

    public void setMeterMaster(MeterMasterInterface meterMaster);

    public List<MeterCTRMappingInterface> getMeterCTRMappings();

    public void setMeterCTRMappings(List<MeterCTRMappingInterface> meterCTRMappings);


}
