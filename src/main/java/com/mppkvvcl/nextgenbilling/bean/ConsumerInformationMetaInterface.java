package com.mppkvvcl.nextgenbilling.bean;

import java.util.Date;

/**
 * Created by RUPALI on 9/22/2017.
 */
public interface ConsumerInformationMetaInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public String getPropertyName();

    public void setPropertyName(String propertyName);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);


}

