package com.mppkvvcl.nextgenbilling.bean;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface MeteringTypeInterface extends BeanInterface {

    public static final String METERING_STATUS_METERED = "METERED";

    public static final String METERING_STATUS_UNMETERED = "UNMETERED";

    public long getId();

    public void setId(long id);

    public String getType();

    public void setType(String type);

}
