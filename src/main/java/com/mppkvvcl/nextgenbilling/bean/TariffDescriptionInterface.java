package com.mppkvvcl.nextgenbilling.bean;

public interface TariffDescriptionInterface extends BeanInterface {

    public static final String TARIFF_DESCRIPTION_DOMESTIC = "DOMESTIC";

    public static final String TARIFF_DESCRIPTION_AGRICULTURE = "AGRICULTURE";

    public String getTariffDescription();

    public void setTariffDescription(String tariffDescription);

    public long getId();

    public void setId(long id);

    public String getTariffCategory();

    public void setTariffCategory(String tariffCategory);
}
