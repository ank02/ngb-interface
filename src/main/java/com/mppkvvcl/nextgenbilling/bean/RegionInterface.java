package com.mppkvvcl.nextgenbilling.bean;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface RegionInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public String getName();

    public void setName(String name);

    public DiscomInterface getDiscom();

    public void setDiscom(DiscomInterface discomInterface);
}
