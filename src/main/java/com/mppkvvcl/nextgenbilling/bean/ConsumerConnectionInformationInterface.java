package com.mppkvvcl.nextgenbilling.bean;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by RUPALI on 9/22/2017.
 */
public interface ConsumerConnectionInformationInterface extends BeanInterface{

    public static final String PHASE_SINGLE = "SINGLE";

    public static final String METERING_STATUS_METERED = "METERED";

    public static final String METERING_STATUS_UNMETERED = "UNMETERED";

    public static final String IS_SEASONAL = "Y";

    public long getId();

    public void setId(long id);

    public String getConsumerNo();

    public void setConsumerNo(String consumerNo);

    public String getTariffCategory();

    public void setTariffCategory(String tariffCategory);

    public String getConnectionType();

    public void setConnectionType(String connectionType);

    public String getMeteringStatus();

    public void setMeteringStatus(String meteringStatus);

    public String getPremiseType();

    public void setPremiseType(String premiseType);

    public BigDecimal getSanctionedLoad();

    public void setSanctionedLoad(BigDecimal sanctionedLoad);

    public String getSanctionedLoadUnit();

    public void setSanctionedLoadUnit(String sanctionedLoadUnit);

    public BigDecimal getContractDemand();

    public void setContractDemand(BigDecimal contractDemand);

    public String getContractDemandUnit();

    public void setContractDemandUnit(String contractDemandUnit);

    public String getIsSeasonal();

    public void setIsSeasonal(String isSeasonal);

    public Long getPurposeOfInstallationId();

    public void setPurposeOfInstallationId(Long purposeOfInstallationId);

    public String getPurposeOfInstallation();

    public void setPurposeOfInstallation(String purposeOfInstallation);

    public String getTariffCode();

    public void setTariffCode(String tariffCode);

    public long getSubCategoryCode();

    public void setSubCategoryCode(long subCategoryCode);

    public String getPhase();

    public void setPhase(String phase);

    public String getIsGovernment();

    public void setIsGovernment(String isGovernment);

    public BigDecimal getPlotSize();

    public void setPlotSize(BigDecimal plotSize);

    public String getPlotSizeUnit();

    public void setPlotSizeUnit(String plotSizeUnit);

    public String getLocationCode();

    public void setLocationCode(String locationCode);

    public String getIsXray();

    public void setIsXray(String isXray);

    public String getIsWeldingTransformerSurcharge();

    public void setIsWeldingTransformerSurcharge(String isWeldingTransformerSurcharge);

    public String getIsCapacitorSurcharge();

    public void setIsCapacitorSurcharge(String isCapacitorSurcharge);

    public String getIsDemandside();

    public void setIsDemandside(String isDemandside);

    public String getIsBeneficiary();

    public void setIsBeneficiary(String isBeneficiary);

    public Date getConnectionDate();

    public void setConnectionDate(Date connectionDate);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getUpdatedOn();

    public void setUpdatedOn(Date updatedOn);
    public String getUpdatedBy();

    public void setUpdatedBy(String updatedBy);


}
