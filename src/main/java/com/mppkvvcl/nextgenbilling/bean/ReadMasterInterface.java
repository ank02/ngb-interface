package com.mppkvvcl.nextgenbilling.bean;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface ReadMasterInterface extends BeanInterface {

    public static final String REPLACEMENT_FLAG_NORMAL_READ = "NR";

    public static final String REPLACEMENT_FLAG_FINAL_READ = "FR";

    public static final String REPLACEMENT_FLAG_START_READ = "SR";

    public static final String REPLACEMENT_FLAG_CTR_READ = "CT";

    public static final String REPLACEMENT_FLAG_NEW_CONNECTION = "NC";

    public static final String SOURCE_MANUAL = "MANUAL";

    public static final String READING_TYPE_METER_RESTART = "METER_RESTART";

    public static final String READING_TYPE_PFL = "PFL";

    public static final String READING_TYPE_NORMAL = "NORMAL";

    public static final String READING_TYPE_ASSESSMENT = "ASSESSMENT";

    public static final String METER_STATUS_WORKING = "WORKING";

    public static final long FOUR_DIGIT_METER_MAX_VALUE = 9999;

    public static final long FIVE_DIGIT_METER_MAX_VALUE = 99999;

    public static final long SIX_DIGIT_METER_MAX_VALUE = 999999;

    public static final boolean USED_ON_BILL = true;

    public static final boolean NOT_USED_ON_BILL = false;

    public long getId();

    public void setId(long id);

    public String getBillMonth();

    public void setBillMonth(String billMonth);

    public String getGroupNo();

    public void setGroupNo(String groupNo);

    public String getReadingDiaryNo();

    public void setReadingDiaryNo(String readingDiaryNo);

    public String getConsumerNo();

    public void setConsumerNo(String consumerNo);

    public String getMeterIdentifier();

    public void setMeterIdentifier(String meterIdentifier);

    public Date getReadingDate();

    public void setReadingDate(Date readingDate);

    public String getReadingType();

    public void setReadingType(String readingType);

    public String getMeterStatus();

    public void setMeterStatus(String meterStatus);

    public String getReplacementFlag();

    public void setReplacementFlag(String replacementFlag);

    public String getSource();

    public void setSource(String source);

    public BigDecimal getReading();

    public void setReading(BigDecimal reading);

    public BigDecimal getDifference();

    public void setDifference(BigDecimal difference);

    public BigDecimal getMf();

    public void setMf(BigDecimal mf);

    public BigDecimal getConsumption();

    public void setConsumption(BigDecimal consumption);

    public BigDecimal getAssessment();

    public void setAssessment(BigDecimal assessment);

    public BigDecimal getPropagatedAssessment();

    public void setPropagatedAssessment(BigDecimal propagatedAssessment);

    public BigDecimal getTotalConsumption();

    public void setTotalConsumption(BigDecimal totalConsumption);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getUpdatedBy();

    public void setUpdatedBy(String updatedBy);

    public Date getUpdatedOn();

    public void setUpdatedOn(Date updatedOn);

    public  ReadMasterKWInterface getReadMasterKW();

    public void setReadMasterKW(ReadMasterKWInterface readMasterKW);

    public ReadMasterPFInterface getReadMasterPF();

    public void setReadMasterPF(ReadMasterPFInterface readMasterPF);

    public String getRemark();

    public void setRemark(String remark);

    public boolean isUsedOnBill();

    public void setUsedOnBill(boolean usedOnBill);

}
