package com.mppkvvcl.nextgenbilling.bean;

import java.util.Date;

/**
 * Created by RUPALI on 9/22/2017.
 */
public interface ConsumerConnectionInformationHistoryInterface extends BeanInterface{
    public void setId(long id);

    public long getId();

    public String getConsumerNo();

    public void setConsumerNo(String consumerNo);

    public String getPropertyName();

    public void setPropertyName(String propertyName);

    public String getPropertyValue();

    public void setPropertyValue(String propertyValue);

    public Date getEndDate();

    public void setEndDate(Date endDate);

    public String getEndBillMonth();

    public void setEndBillMonth(String endBillMonth);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);


}
