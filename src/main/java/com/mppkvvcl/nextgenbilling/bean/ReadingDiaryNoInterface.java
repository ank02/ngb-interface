package com.mppkvvcl.nextgenbilling.bean;

public interface ReadingDiaryNoInterface extends BeanInterface {


    public long getId();

    public void setId(long id);

    public String getGroupNo();

    public void setGroupNo(String groupNo);

    public String getReadingDiaryNo();

    public void setReadingDiaryNo(String readingDiaryNo);
}
