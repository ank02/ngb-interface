package com.mppkvvcl.nextgenbilling.bean;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by RUPALI on 9/22/2017.
 */
public interface ElectricityDutyInterface extends BeanInterface {
    public Long getId();

    public void setId(Long id);

    public Long getSubcategoryCode();

    public void setSubcategoryCode(Long subCategoryCode);

    public Long getStartConsumption();

    public void setStartConsumption(Long startConsumption);

    public Long getEndConsumption();

    public void setEndConsumption(Long endConsumption);

    public BigDecimal getRate();

    public void setRate(BigDecimal rate);

    public Date getEffectiveStartDate();

    public void setEffectiveStartDate(Date effectiveStartDate);

    public Date getEffectiveEndDate();

    public void setEffectiveEndDate(Date effectiveEndDate);

    public BigDecimal getMultiplier();

    public void setMultiplier(BigDecimal multiplier);

    public String getAppVersion() ;

    public void setAppVersion(String appVersion);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getUpdatedBy();

    public void setUpdatedBy(String updatedBy);

    public Date getUpdatedOn();

    public void setUpdatedOn(Date updatedOn);

}
