package com.mppkvvcl.nextgenbilling.bean;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface ISIEnergySavingTypeInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public String getType();

    public void setType(String type);

    public String getDescription();

    public void setDescription(String description);
}
