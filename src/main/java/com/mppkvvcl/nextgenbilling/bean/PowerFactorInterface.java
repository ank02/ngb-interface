package com.mppkvvcl.nextgenbilling.bean;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface PowerFactorInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public String getChargeCode();

    public void setChargeCode(String chargeCode);

    public BigDecimal getStartRange();

    public void setStartRange(BigDecimal startRange);

    public BigDecimal getEndRange();

    public void setEndRange(BigDecimal endRange);

    public BigDecimal getRate();

    public void setRate(BigDecimal rate);

    public Date getStartDate();

    public void setStartDate(Date startDate);

    public Date getEndDate();

    public void setEndDate(Date endDate);

    public BigDecimal getMultiplier();

    public void setMultiplier(BigDecimal multiplier);

}
