package com.mppkvvcl.nextgenbilling.bean;

import java.util.List;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface TariffInterface extends BeanInterface {

    public long getId();

    public void setId(long id);

    public String getTariffType();

    public void setTariffType(String tariffType);

    public String getTariffCategory();

    public void setTariffCategory(String tariffCategory);

    public String getTariffCode();

    public void setTariffCode(String tariffCode);

    public String getEffectiveStartDate();

    public void setEffectiveStartDate(String effectiveStartDate);

    public String getEffectiveEndDate();

    public void setEffectiveEndDate(String effectiveEndDate);

    public String getMeteringStatus();

    public void setMeteringStatus(String meteringStatus);

    public String getConnectionType();

    public void setConnectionType(String connectionType);

    public String getDescription();

    public void setDescription(String description);

    public String getCreatedOn();

    public void setCreatedOn(String createdOn);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public String getUpdatedOn();

    public void setUpdatedOn(String updatedOn);

    public String getUpdatedBy();

    public void setUpdatedBy(String updatedBy);

    public void setSubCategoryInterface(SubCategoryInterface subCategoryInterface);

    public SubCategoryInterface getSubCategoryInterface();

    public void setAlternateSubCategoryInterface(SubCategoryInterface alternateSubCategoryInterface);

    public SubCategoryInterface getAlternateSubCategoryInterface();
}
