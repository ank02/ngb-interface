package com.mppkvvcl.nextgenbilling.bean;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface RoleInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public String getRole();

    public void setRole(String role);
}
