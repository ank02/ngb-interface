package com.mppkvvcl.nextgenbilling.bean;

import java.util.Date;

/**
 * Created by RUPALI on 9/22/2017.
 */
public interface CashWindowStatusInterface extends BeanInterface {

    public static final String STATUS_OPEN = "OPEN";

    public static final String STATUS_CLOSE = "CLOSE";

    public long getId();

    public void setId(long id);

    public String getLocationCode();

    public void setLocationCode(String locationCode);

    public String getUsername();

    public void setUsername(String username);

    public String getWindowName();

    public void setWindowName(String windowName);

    public Date getDate();

    public void setDate(Date date);

    public String getStatus();

    public void setStatus(String status);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public String getUpdatedBy();

    public void setUpdatedBy(String updatedBy);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public Date getUpdatedOn();

    public void setUpdatedOn(Date updatedOn);

    public UserDetailInterface getUserDetail();

    public void setUserDetails(UserDetailInterface userDetails);

}
