package com.mppkvvcl.nextgenbilling.bean;
import java.util.Date;

public interface UserInterface extends BeanInterface {

    public long getId() ;

    public void setId(long id);

    public String getUsername() ;

    public void setUsername(String username);

    public String getPassword();

    public void setPassword(String password) ;

    public String getRole() ;

    public void setRole(String role) ;

    public String getStatus() ;

    public void setStatus(String status) ;

    public void setCreatedOn(Date createdOn);

    public Date getCreatedOn();

    public void setUpdatedOn(Date updatedOn);

    public Date getUpdatedOn();

}
