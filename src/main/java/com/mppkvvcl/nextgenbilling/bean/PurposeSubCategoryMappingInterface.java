package com.mppkvvcl.nextgenbilling.bean;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface PurposeSubCategoryMappingInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public long getPurposeOfInstallationId();

    public void setPurposeOfInstallationId(long purposeOfInstallationId);

    public long getSubcategoryCode();

    public void setSubcategoryCode(long subcategoryCode);

}
