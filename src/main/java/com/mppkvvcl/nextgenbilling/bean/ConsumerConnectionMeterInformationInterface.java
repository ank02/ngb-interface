package com.mppkvvcl.nextgenbilling.bean;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by RUPALI on 9/22/2017.
 */
public interface ConsumerConnectionMeterInformationInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public String getConsumerNo();

    public void setConsumerNo(String consumerNo);

    public String getMeterIdentifier();

    public void setMeterIdentifier(String meterIdentifier);

    public BigDecimal getStartRead();

    public void setStartRead(BigDecimal startRead);

    public String getHasCTR();

    public void setHasCTR(String hasCTR);

    public Date getMeterInstallationDate();

    public void setMeterInstallationDate(Date meterInstallationDate);

    public String getMeterInstallerName();

    public void setMeterInstallerName(String meterInstallerName);

    public String getMeterInstallerDesignation();

    public void setMeterInstallerDesignation(String meterInstallerDesignation);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getUpdatedOn();

    public void setUpdatedOn(Date updatedOn);

    public String getUpdatedBy();

    public void setUpdatedBy(String updatedBy);


}
