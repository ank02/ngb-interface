package com.mppkvvcl.nextgenbilling.bean;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface MeterMasterInterface extends BeanInterface {

    public static final String CODE_TYPE_CTT = "CTT";

    public static final String CODE_TYPE_WCT = "WCT";

    public static final String CODE_TYPE_WCS = "WCS";

    public long getId();

    public void setId(long id);

    public String getIdentifier();

    public void setIdentifier(String identifier);

    public String getSerialNo();

    public void setSerialNo(String serialNo);

    public String getMake();

    public void setMake(String make);

    public String getMeterOwner();

    public void setMeterOwner(String meterOwner);

    public String getCapacity();

    public void setCapacity(String capacity);

    public String getCtr();

    public void setCtr(String ctr);

    public BigDecimal getMf();

    public void setMf(BigDecimal mf);

    public String getDescription();

    public void setDescription(String description);

    public String getPhase();

    public void setPhase(String phase);

    public String getCode();

    public void setCode(String code);

    public String getIsPrepaid();

    public void setIsPrepaid(String isPrepaid);

    public String getHistoryNo();

    public void setHistoryNo(String historyNo);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);
}
