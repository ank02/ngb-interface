package com.mppkvvcl.nextgenbilling.bean;

import java.util.Date;

/**
 * Created by RUPALI on 9/22/2017.
 */
public interface CTRMasterInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public String getIdentifier();

    public void setIdentifier(String identifier);

    public String getSerialNo();

    public void setSerialNo(String serialNo);

    public String getMake();

    public void setMake(String make);

    public String getCtRatio();

    public void setCtRatio(String ctRatio);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);


}
