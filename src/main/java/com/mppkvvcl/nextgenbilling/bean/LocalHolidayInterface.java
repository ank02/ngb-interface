package com.mppkvvcl.nextgenbilling.bean;

import java.util.Date;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface LocalHolidayInterface extends BeanInterface{
    public long getId();

    public void setId(long id);

    public String getLocationCode();

    public void setLocationCode(String locationCode);

    public Date getDate();

    public void setDate(Date date);

    public String getDescription();

    public void setDescription(String description);
}
