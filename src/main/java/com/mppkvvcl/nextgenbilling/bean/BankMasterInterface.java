package com.mppkvvcl.nextgenbilling.bean;

/**
 * Created by RUPALI on 9/22/2017.
 */
public interface BankMasterInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public String getName();

    public void setName(String name);

    public String getCode();

    public void setCode(String code);

}
