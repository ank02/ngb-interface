package com.mppkvvcl.nextgenbilling.bean;

import java.util.Date;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface PartPaymentInterface extends BeanInterface {

    public static final boolean APPLIED_TRUE = true;

    public static final boolean APPLIED_FALSE = false;

    public long getId();

    public void setId(long id);

    public String getConsumerNo();

    public void setConsumerNo(String consumerNo);

    public long getAmount();

    public void setAmount(long amount);

    public String getApprovedBy();

    public void setApprovedBy(String approvedBy);

    public boolean isApplied();

    public void setApplied(boolean applied);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getUpdatedBy();

    public void setUpdatedBy(String updatedBy);

    public Date getUpdatedOn();

    public void setUpdatedOn(Date updatedOn);
}
