package com.mppkvvcl.nextgenbilling.bean;

import java.util.Date;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface NSCStagingStatusInterface extends BeanInterface{
    public long getId();

    public void setId(long id);

    public long getNscStagingId();

    public void setNscStagingId(long nscStagingId);

    public String getStatus();

    public void setStatus(String status);

    public String getGeneratedConsumerNo();

    public void setGeneratedConsumerNo(String generatedConsumerNo);

    public String getRemark();

    public void setRemark(String remark);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public Date getLastUpdatedOn();

    public void setLastUpdatedOn(Date lastUpdatedOn);
}
