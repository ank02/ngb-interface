package com.mppkvvcl.nextgenbilling.bean;

import java.math.BigDecimal;

/**
 * Created by RUPALI on 9/22/2017.
 */
public interface AgricultureUnitInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public long getSubcategoryCode();

    public void setSubcategoryCode(long subcategoryCode);

    public long getTariffId();

    public void setTariffId(long tariffId);

    public long getAlternateSubcategoryCode();

    public void setAlternateSubcategoryCode(long alternateSubcategoryCode);

    public String getPhase();

    public void setPhase(String phase);

    public String getAreaType();

    public void setAreaType(String areaType);

    public String getDescription();

    public void setDescription(String description);
    public String getStartMonth();

    public void setStartMonth(String startMonth);
    public String getEndMonth();

    public void setEndMonth(String endMonth);

    public BigDecimal getBillingUnits();

    public void setBillingUnits(BigDecimal billingUnits);



}
