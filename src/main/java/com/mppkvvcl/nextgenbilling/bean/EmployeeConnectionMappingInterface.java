package com.mppkvvcl.nextgenbilling.bean;

import java.util.Date;

/**
 * Created by RUPALI on 9/22/2017.
 */
public interface EmployeeConnectionMappingInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public String getEmployeeNo();

    public void setEmployeeNo(String employeeNo);

    public String getConsumerNo();

    public void setConsumerNo(String consumerNo);

    public String getStatus();

    public void setStatus(String status);

    public Date getStartDate();

    public void setStartDate(Date startDate);

    public String getStartBillMonth();

    public void setStartBillMonth(String startBillMonth);

    public Date getEndDate();

    public void setEndDate(Date endDate);

    public String getEndBillMonth();

    public void setEndBillMonth(String endBillMonth);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getUpdatedOn();

    public void setUpdatedOn(Date updatedOn);

    public String getUpdatedBy();

    public void setUpdatedBy(String updatedBy);


}
