package com.mppkvvcl.nextgenbilling.bean;

import java.math.BigDecimal;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface IrrigationSchemeInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public String getLocationCode();

    public void setLocationCode(String locationCode);

    public String getConsumerNo();

    public void setConsumerNo(String consumerNo);

    public String getInstallmentBillMonth();

    public void setInstallmentBillMonth(String installmentBillMonth);

    public String getOldInstallmentBillMonth();

    public void setOldInstallmentBillMonth(String oldInstallmentBillMonth);

    public BigDecimal getFrozenArrear();

    public void setFrozenArrear(BigDecimal frozenArrear);

    public BigDecimal getInstallmentAmountMirash();

    public void setInstallmentAmountMirash(BigDecimal installmentAmountMirash);

    public BigDecimal getInstalmentRemaining();

    public void setInstalmentRemaining(BigDecimal instalmentRemaining);

    public BigDecimal getOldInstalmentRemaining();

    public void setOldInstalmentRemaining(BigDecimal oldInstalmentRemaining);

    public BigDecimal getBalanceFrozenJun15();

    public void setBalanceFrozenJun15(BigDecimal balanceFrozenJun15);

    public BigDecimal getInstallmentAmountNew();

    public void setInstallmentAmountNew(BigDecimal installmentAmountNew);
}
