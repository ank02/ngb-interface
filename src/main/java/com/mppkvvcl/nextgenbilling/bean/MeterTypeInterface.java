package com.mppkvvcl.nextgenbilling.bean;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface MeterTypeInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public String getMeterDescription();

    public void setMeterDescription(String meterDescription);

    public String getMeterPhase();

    public void setMeterPhase(String meterPhase);

    public String getMeterCode();

    public void setMeterCode(String meterCode);
}
