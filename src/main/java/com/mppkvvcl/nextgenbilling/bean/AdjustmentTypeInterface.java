package com.mppkvvcl.nextgenbilling.bean;

/**
 * Created by RUPALI on 9/22/2017.
 */
public interface AdjustmentTypeInterface extends BeanInterface {

    public static final boolean MANUAL_ADJUSTMENT_TRUE = true;

    public static final boolean MANUAL_ADJUSTMENTS_FALSE = false;

    public long getId();

    public void setId(long id);

    public int getCode();

    public void setCode(int code);

    public String getDetail();

    public void setDetail(String detail);

    public String getR15Mapping();

    public void setR15Mapping(String r15Mapping);

    public boolean isEffectOnCurrentBill();

    public void setEffectOnCurrentBill(boolean effectOnCurrentBill);

    public boolean isView();

    public void setView(boolean view);

    public boolean isDebit();

    public void setDebit(boolean debit);

    public boolean isCredit();

    public void setCredit(boolean credit);

    public long getMinimum();

    public void setMinimum(long minimum) ;

    public long getMaximum() ;

    public void setMaximum(long maximum) ;

    public long getDefaultValue() ;

    public void setDefaultValue(long defaultValue);
}
