package com.mppkvvcl.nextgenbilling.bean;

import java.util.Date;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface ScheduleInterface extends BeanInterface {

    public static final String BILL_STATUS_PENDING = "PENDING";

    public static final String BILL_STATUS_COMPLETED = "COMPLETED";

    public static final int DIFFERENCE_BETWEEN_BILL_DATE_AND_DUE_DATE  = 15;

    public static final int DIFFERENCE_BETWEEN_BILL_DATE_AND_CHEQUE_DUE_DATE  = 12;

    public static final int DIFFERENCE_BETWEEN_SCHEDULE  = 15;

    public static final String SUBMITTED_Y = "Y";

    public static final String SUBMITTED_N = "N";

    public static final String R15_STATUS_FROZEN = "FROZEN";

    public static final String R15_STATUS_UNFREEZE = "UNFREEZE";

    public long getId();

    public void setId(long id);

    public String getGroupNo();

    public void setGroupNo(String groupNo);

    public String getBillMonth();

    public void setBillMonth(String billMonth);

    public String getBillStatus();

    public void setBillStatus(String billStatus);

    public String getSubmitted();

    public void setSubmitted(String submitted);

    public String getR15Status();

    public void setR15Status(String r15Status);

    public Date getChequeDueDate();

    public void setChequeDueDate(Date chequeDueDate);

    public Date getDueDate();

    public void setDueDate(Date dueDate);

    public Date getBillDate();

    public void setBillDate(Date billDate);

    public Date getStartReadingDate();

    public void setStartReadingDate(Date startReadingDate);

    public Date getCashUpto();

    public void setCashUpto(Date cashUpto);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getUpdatedBy();

    public void setUpdatedBy(String updatedBy);

    public Date getUpdatedOn();

    public void setUpdatedOn(Date updatedOn);

    public Date getEndReadingDate();

    public void setEndReadingDate(Date endReadingDate);

}
