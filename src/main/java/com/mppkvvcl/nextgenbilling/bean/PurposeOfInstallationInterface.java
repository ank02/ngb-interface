package com.mppkvvcl.nextgenbilling.bean;

import java.math.BigDecimal;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface PurposeOfInstallationInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public String getTariffCode();

    public void setTariffCode(String tariffCode);

    public String getPurposeOfInstallation();

    public void setPurposeOfInstallation(String purposeOfInstallation);

    public String getLoadUnit();

    public void setLoadUnit(String loadUnit);

    public BigDecimal getMaxLoad();

    public void setMaxLoad(BigDecimal maxLoad);
}
