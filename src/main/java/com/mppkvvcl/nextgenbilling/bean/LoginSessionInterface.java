package com.mppkvvcl.nextgenbilling.bean;

import java.util.Date;

public interface LoginSessionInterface extends BeanInterface {

        public long getId() ;

        public void setId(long id) ;

        public String getUsername();

        public void setUsername(String username);

        public Date getLogin();

        public void setLogin(Date login);

        public Date getLogout() ;

        public void setLogout(Date logout) ;

        public String getIp();

        public void setIp(String ip);

        public String getBrowserName();

        public void setBrowserName(String browserName);

        public String getBrowserVersion();

        public void setBrowserVersion(String browserVersion) ;


}
