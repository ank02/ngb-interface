package com.mppkvvcl.nextgenbilling.bean;

import java.util.Date;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface ReadTypeConfiguratorInterface extends BeanInterface{
    public long getId();

    public void setId(long id);

    public long getTariffId();

    public void setTariffId(long tariffId);

    public long getSubcategoryCode();

    public void setSubcategoryCode(long subcategoryCode);

    public boolean isKwh();

    public void setKwh(boolean kwh);

    public boolean isKw();

    public void setKw(boolean kw);

    public boolean isPf();

    public void setPf(boolean pf);

    public boolean isKva();

    public void setKva(boolean kva);

    public boolean isKvah();

    public void setKvah(boolean kvah);

    public boolean isTod1();

    public void setTod1(boolean tod1);

    public boolean isTod2();

    public void setTod2(boolean tod2);

    public boolean isTod3();

    public void setTod3(boolean tod3);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getUpdatedBy();

    public void setUpdatedBy(String updatedBy);

    public Date getUpdatedOn();

    public void setUpdatedOn(Date updatedOn);
}
