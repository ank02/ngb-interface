package com.mppkvvcl.nextgenbilling.bean;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface PostMeterReplacementInterface extends BeanInterface {
    public ConsumerMeterMappingInterface getConsumerMeterMapping();

    public void setConsumerMeterMapping(ConsumerMeterMappingInterface consumerMeterMappingInterface);

    public MeterCTRMappingInterface getMeterCTRMapping();

    public void setMeterCTRMapping(MeterCTRMappingInterface meterCTRMappingInterface);

    public ReadMasterInterface getFinalRead();

    public void setFinalRead(ReadMasterInterface finalRead);

    public ReadMasterInterface getStartRead();

    public void setStartRead(ReadMasterInterface startRead);

    public ReadMasterInterface getCurrentRead();

    public void setCurrentRead(ReadMasterInterface currentRead);

}
