package com.mppkvvcl.nextgenbilling.bean;

import java.util.Date;

/**
 * Created by RUPALI on 9/22/2017.
 */
public interface ConsumerInformationInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public String getConsumerNo();

    public void setConsumerNo(String consumerNo);

    public String getConsumerName();

    public void setConsumerName(String consumerName);

    public String getConsumerNameH();

    public void setConsumerNameH(String consumerNameH);

    public String getRelativeName();

    public void setRelativeName(String relativeName);

    public String getRelation();

    public void setRelation(String relation);

    public String getIsBPL();

    public void setIsBPL(String isBPL);

    public String getCategory();

    public void setCategory(String category);

    public String getIsEmployee();

    public void setIsEmployee(String isEmployee);

    public String getAddress1();

    public void setAddress1(String address1);

    public String getAddress2();

    public void setAddress2(String address2);

    public String getAddress3();

    public void setAddress3(String address3);

    public String getAddress1H();

    public void setAddress1H(String address1H);

    public String getAddress2H();

    public void setAddress2H(String address2H);

    public String getAddress3H();

    public void setAddress3H(String address3H);

    public String getPrimaryMobileNo();

    public void setPrimaryMobileNo(String primaryMobileNo);

    public String getAlternateMobileNo();

    public void setAlternateMobileNo(String alternateMobileNo);
    public String getAadhaarNo();

    public void setAadhaarNo(String aadhaarNo);

    public String getPan();

    public void setPan(String pan);

    public String getBankAccountNo();

    public void setBankAccountNo(String bankAccountNo);

    public String getBankAccountHolderName();

    public void setBankAccountHolderName(String bankAccountHolderName);

    public String getBankName();

    public void setBankName(String bankName);

    public String getIfsc();

    public void setIfsc(String ifsc);

    public String getEmailAddress();

    public void setEmailAddress(String emailAddress);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getUpdatedBy();
    public void setUpdatedBy(String updatedBy);

    public Date getUpdatedOn();

    public void setUpdatedOn(Date updatedOn);


}
