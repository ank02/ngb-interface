package com.mppkvvcl.nextgenbilling.bean;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface MeterRentInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public String getMeterCode();

    public void setMeterCode(String meterCode);

    public String getMeterCapacity();

    public void setMeterCapacity(String meterCapacity);

    public BigDecimal getMeterRent();

    public void setMeterRent(BigDecimal meterRent);

    public Date getEffectiveStartDate();

    public void setEffectiveStartDate(Date effectiveStartDate);

    public Date getEffectiveEndDate();

    public void setEffectiveEndDate(Date effectiveEndDate);
}

